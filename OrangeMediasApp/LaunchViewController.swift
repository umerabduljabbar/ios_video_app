//
//  LaunchViewController.swift
//  OrangeMediasApp
//
//  Created by Umer Jabbar on 11/03/2018.
//  Copyright © 2018 ZotionApps. All rights reserved.
//

import UIKit
import Player
import Alamofire
import AlamofireObjectMapper

class LaunchViewController: UIViewController {
    
    var anyUpdate = false
    var pathArray = [String]()
    var image = Keys.imageLink
    
    var player = Player()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.player.playerDelegate = self
        self.player.playbackDelegate = self
        self.player.view.frame = self.view.bounds
        
        self.addChildViewController(self.player)
        self.view.addSubview(self.player.view)
        self.player.didMove(toParentViewController: self)
        
        
        //        let videoUrl: URL = NSURL(fileURLWithPath: "/Users/umer/Develop/OrangeMediasApp/OrangeMediasApp/splash.mp4") as URL
        guard let videoUrl1 = Bundle.main.path(forResource: "splash", ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }
        self.player.url = NSURL(fileURLWithPath: videoUrl1) as URL
        
        self.player.playFromBeginning()
        
        self.player.fillMode = PlayerFillMode.resizeAspectFit.avFoundationType
        
        
        for i in Keys.pathLink {
            isExists(paths: i)
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        sendRequestRequest()
    
    }
    
    func sendRequestRequest() {
        /**
         Request
         get http://5thfloor.media/app/api.video/n1/Video/get_ios_app_version
         */
        
        // Add Headers
        let headers = [
            "Cookie":"__cfduid=d9a5e2e9e9dbd3c0ede79fa2179344afe1520780675; ci_session=855db92f2d6340cefd7be0e611045ed3980593f6",
            ]
        
        // Fetch Request
        Alamofire.request("http://5thfloor.media/app/api.video/n1/Video/get_ios_app_version", method: .get, headers: headers)
            .validate(statusCode: 200..<300)
            .responseObject{ (response: DataResponse<AppVersion>) in
                
                let appversion = response.result.value
                
                
                if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                    if (Float(appversion?.getUserAppRelease?.first?.version ?? "1.0")! > Float(version)!){
                        self.anyUpdate = true
                    }
                }
                
        }
        
    }


    func isExists(paths : String) -> Bool{
        
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = FileManager()
        let file_name = NSURL(fileURLWithPath:  paths).lastPathComponent
        let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/\(file_name ?? "")"))
//        print(file_name)
        
        if fileManager.fileExists(atPath: destinationURLForFile.path){
            self.pathArray.append(destinationURLForFile.path)
            return true
        }
        
        return false
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if "playDIrect" == segue.identifier {
            if let destination = segue.destination as? PlayerViewController{
                destination.pathArray = self.pathArray
                destination.image = self.image
            }
        }
    }

}

extension LaunchViewController : PlayerDelegate{
    
    func playerReady(_ player: Player) {
     print("Player Ready")
    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferTimeDidChange(_ bufferTime: Double) {
        
    }
    
    
}

extension LaunchViewController : PlayerPlaybackDelegate{
    
    func playerCurrentTimeDidChange(_ player: Player) {
        
    }
    
    func playerPlaybackWillStartFromBeginning(_ player: Player) {
        
    }
    
    func changeVC(vc: UIViewController) {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        vc.view.frame = rootViewController.view.frame
        vc.view.layoutIfNeeded()
        
        UIView.transition(with: window, duration: 0.3, options: .transitionFlipFromBottom, animations: {
            window.rootViewController = vc
        }, completion: { completed in
            // maybe do something here
        })
    }
    
    func playerPlaybackDidEnd(_ player: Player) {
        print("Player playback Did End")
        
        if(!anyUpdate){
            if(Keys.installed){
//                performSegue(withIdentifier: "playDIrect", sender: self)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
                vc.pathArray = self.pathArray
                vc.image = self.image
                changeVC(vc: vc)
            }else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                 changeVC(vc: vc)
            }
        
        }else{
        
            let alert = UIAlertController(title: "Update Available", message: "To continue using this app you need to update it.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Update", style: .default, handler: nil))
//            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
    
    func playerPlaybackWillLoop(_ player: Player) {
        
    }
    
    
}
