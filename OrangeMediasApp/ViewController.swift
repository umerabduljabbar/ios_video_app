//
//  ViewController.swift
//  OrangeMediasApp
//
//  Created by Umer Jabbar on 10/03/2018.
//  Copyright © 2018 ZotionApps. All rights reserved.
//

import UIKit
import Player
import Alamofire
import AlamofireObjectMapper
import Moya
import Photos
import AVFoundation
import AVKit


var videoUrl = URL(string: "http://content.jwplatform.com/videos/gHvARMPz-MU1I6cB0.mp4")!

class ViewController: UIViewController, URLSessionDownloadDelegate {
    
    @IBOutlet var TextBox: UITextField!
    @IBOutlet var NumberLable: UILabel!
    var image: String?
    
    @IBOutlet var downloadBackgroundColor: UIView!
    @IBOutlet var percentageLabel: UILabel!
    
    var pathArray = [String]()
    var linkArray = [String]()
    var count = 0;
    
    @IBAction func exitBtn(_ sender: UIButton) {
        
        
        
    }
    
    var playerViewController = AVPlayerViewController()
    var playerView = AVPlayer()
    
    var downloadTask: URLSessionDownloadTask!
    var backgroundSession: URLSession! {
        
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
        return Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)

    }
    
    func downloadAll(){

        print("Download did start")
        downloadBackgroundColor.isHidden = false
        if(!linkArray.isEmpty){
            if(linkArray.count > count){
                
                if(isExists(paths: linkArray[count])){
                    
                    print("Already in path ")
                    count += 1
                    downloadAll()
                    
                }else{
                    Keys.downloadedVideos += 1
                    self.NumberLable.text = "\(Keys.downloadedVideos)/ \(Keys.totalVideoCount)"
                    downloadTask = backgroundSession.downloadTask(with: URL(string: linkArray[count])!)
                    downloadTask.resume()
                }
                
            }else{
                print("All Downloaded")
                
                Keys.installed = true
//                self.dismiss(animated: true, completion: nil)
//                performSegue(withIdentifier: "play", sender: self)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
                vc.pathArray = self.pathArray
                vc.image = self.image
                changeVC(vc: vc)
                
            }
        }
    }
    
    func changeVC(vc: UIViewController) {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        vc.view.frame = rootViewController.view.frame
        vc.view.layoutIfNeeded()
        
        UIView.transition(with: window, duration: 0.3, options: .transitionFlipFromBottom, animations: {
            window.rootViewController = vc
        }, completion: { completed in
            // maybe do something here
        })
    }
    
    @IBAction func doEndText(_ sender: UITextField) {
        
        sender.returnKeyType = .done
        view.endEditing(true)
        
    }
    
    @IBAction func startDownload(_ sender: AnyObject) {

        print("Download did tap")
        
        if(!(self.TextBox.text?.isEmpty)!){
            
            if(Keys.appcode != self.TextBox.text!){
                Keys.downloadedVideos = 0
                for item in pathArray{
                    try? FileManager.default.removeItem(atPath: item)
                }
            }
            
            Keys.appcode = self.TextBox.text!
            
            
            let URL = "http://5thfloor.media/app/api.video/n1/Video/get_video_category?app_id=\(self.TextBox.text ?? "")"
            
            Alamofire.request(URL).responseObject { (response: DataResponse<VideoCategory>) in
                
                let videos = response.result.value
                
                if(videos?.status ?? false){
                    
                if(response.result.isFailure){
                    
                    let alert = UIAlertController(title: "Wrong AppCode", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: nil))

                    self.present(alert, animated: true)
                    
                    return
                    
                }
                self.image = videos?.bannerLink?.first?.imgPath ?? ""
                Keys.imageLink = self.image!
                
                Keys.totalVideoCount = (videos?.breakVideo?.count ?? 0) + (videos?.data?.count ?? 0)
                
                if(videos != nil){
                    
                    self.sendDeviceStatus()
                    
                    var dyn = [String]()
                    let no = videos?.breakVideo?.count ?? 0
                    for i in 0..<no {
                        for j in 0...((Int(videos?.breakVideo![i].count ?? "0") ?? 0) - 1) {
                            
                                let vUrl = videos?.data?.first?.vUrl
                                let dataItem = (videos?.data?.removeFirst())!
                                videos?.data?.append(dataItem)
                                dyn.append(vUrl!)
                                print("i = \(i), j = \(j), videolink = \(vUrl ?? "nothing")")
                            
                        }
                        
                        dyn.append((videos?.breakVideo![i].vUrl)!)
                        print("i = \(i), videolink = \(videos?.breakVideo![i].vUrl ?? "nothing")")
                    }
                    self.linkArray = dyn
                    Keys.pathLink = dyn
                    
                    self.NumberLable.text = "\(Keys.downloadedVideos)/ \(Keys.totalVideoCount)"
                    
                    
                    //To continue Download
                    Keys.continueDownload = true
                    
                    self.downloadAll()
                }
                }else{
                    
                    let alert = UIAlertController(title: "Wrong AppCode", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: nil))
                    
                    self.present(alert, animated: true)
                    
                    return
                    
                }
            }
        }else{
            
            let alert = UIAlertController(title: "Something Wrong", message: "Check Your Code or try contacting app provider", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
        }
        
    }
    @IBAction func pause(_ sender: AnyObject) {
        if downloadTask != nil{
            downloadTask.suspend()
        }
    }
    @IBAction func resume(_ sender: AnyObject) {
        if downloadTask != nil{
            downloadTask.resume()
        }
    }
    @IBAction func cancel(_ sender: AnyObject) {
        if downloadTask != nil{
            downloadTask.cancel()
        }
        
    }
    @IBOutlet var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TextBox.delegate = self
        progressView?.setProgress(0.0, animated: false)
        percentageLabel.text = "\(0)%"
        
        if(Keys.continueDownload){
            
            self.downloadBackgroundColor.isHidden = false
            
            self.count = Keys.downloadedVideos
            
            self.linkArray = Keys.pathLink
            
            self.NumberLable.text = "\(Keys.downloadedVideos)/ \(Keys.totalVideoCount)"
            
            self.downloadAll()
        }
        
        
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presentingViewController?.removeFromParentViewController()
        
        
    }
    
    func isExists(paths : String) -> Bool{
        
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = FileManager()
        let file_name = NSURL(fileURLWithPath:  paths).lastPathComponent
        let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/\(file_name ?? "car")"))
        
        if fileManager.fileExists(atPath: destinationURLForFile.path){
            pathArray.append(destinationURLForFile.path)
            return true
        }
        
        return false
    }
    
    func showFileWithPath(path: String){
        
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true{
            //            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            //            viewer.delegate = self
            //            viewer.presentPreview(animated: true)
            //
            //            let fileUrl = NSURL(fileURLWithPath: path )
            //            playerView = AVPlayer(url: fileUrl as URL)
            //            playerViewController.player = playerView
            //
            //            self.present(playerViewController, animated: true, completion: {
            //                self.playerViewController.player?.play()
            //            })
            self.pathArray.append(path)
            print("append in array")
            self.count += 1
            
        }
        
    }
    
    //MARK: URLSessionDownloadDelegate
    // 1
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL){
        
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = FileManager()
        let file_name = NSURL(fileURLWithPath:  self.linkArray[count]).lastPathComponent
        let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/\(file_name ?? "car")"))
        print(file_name ?? "")
        
        if fileManager.fileExists(atPath: destinationURLForFile.path){
            showFileWithPath(path: destinationURLForFile.path)
            print("exists")
        }
        else{
            do {
                try fileManager.moveItem(at: location, to: destinationURLForFile)
                // show file
                showFileWithPath(path: destinationURLForFile.path)
            }catch{
                print("An error occurred while moving file to destination url")
            }
        }
    }
    // 2
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64){
        progressView.setProgress(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite), animated: true)
        percentageLabel.text = "\(Int((Float(totalBytesWritten)/Float(totalBytesExpectedToWrite))*100))%"
    }
    
    //MARK: URLSessionTaskDelegate
    func urlSession(_ session: URLSession,
                    task: URLSessionTask,
                    didCompleteWithError error: Error?){
        downloadTask = nil
        progressView.setProgress(0.0, animated: true)
        percentageLabel.text = "\(0)%"
        if (error != nil) {
            print(error!.localizedDescription)
        }else{
            print("The task finished transferring data successfully")
            
            
            downloadAll()
        }
    }
    
//    //MARK: UIDocumentInteractionControllerDelegate
//    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
//    {
//        return self
//    }
    
    
    func sendDeviceStatus() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
        let formatterDate = dateFormatter.string(from: Date())
        let headers = [
            "Cookie":"__cfduid=d9a5e2e9e9dbd3c0ede79fa2179344afe1520780675; ci_session=9d8fedc405a3ac6a2b92ec0a199a916c36e91923",
            "Content-Type":"application/json; charset=utf-8",
            ]
        let body: [String : Any] = [
            "status": Keys.status,
            "date_time": formatterDate,
            "appcode": Keys.appcode
        ]
        print("Video Overview \n")
        print(body)
        
        Alamofire.request("http://5thfloor.media/app/api.video/n1/Util/video_overview", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    print("response success overview")
                }
                else {
                    print("response failed overview")
                }
        }
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if "play" == segue.identifier {
            if let destination = segue.destination as? PlayerViewController{
                destination.pathArray = self.pathArray
                destination.image = self.image
            }
        }
    }
    
}






extension ViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return false
    }
}




