//
//  AppDelegate.swift
//  OrangeMediasApp
//
//  Created by Umer Jabbar on 10/03/2018.
//  Copyright © 2018 ZotionApps. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        print("applicationWillResignActive")
        
    }
    
    func sendRequestRequest() {
        let headers = [
            "Cookie":"__cfduid=d9a5e2e9e9dbd3c0ede79fa2179344afe1520780675; ci_session=54a88976bbda2278c735d3643ae4a22dc03b81a2",
            "Content-Type":"application/json; charset=utf-8",
            ]
        let body: [String : Any] = [
            "appcode": Keys.appcode,
            "device_code": Keys.device_code,
            "start_time": Keys.start_time,
            "video_details": Keys.video_details.joined(separator: ","),
            "end_time": Keys.end_time
        ]
        print("Video Records \n")
        print(body)
        Alamofire.request("http://5thfloor.media/app/api.video/n1/Util/video_records", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    print("response success records")
                    Keys.video_details = []
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
                    let formatterDate = dateFormatter.string(from: Date())
                    Keys.start_time = formatterDate
                }
                else {
                    print("response failed records")
            }
        }
    }
    
    func sendDeviceStatus() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
        let formatterDate = dateFormatter.string(from: Date())
        let headers = [
            "Cookie":"__cfduid=d9a5e2e9e9dbd3c0ede79fa2179344afe1520780675; ci_session=9d8fedc405a3ac6a2b92ec0a199a916c36e91923",
            "Content-Type":"application/json; charset=utf-8",
            ]
        let body: [String : Any] = [
            "status": Keys.status,
            "date_time": formatterDate,
            "appcode": Keys.appcode
        ]
        print("Video Overview \n")
        print(body)

        Alamofire.request("http://5thfloor.media/app/api.video/n1/Util/video_overview", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    print("response success overview")
                }
                else {
                    print("response failed overview")
            }
        }
    }
    

    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("applicationDidEnterBackground")
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
//        let formatterDate = dateFormatter.string(from: Date())
//        Keys.end_time = formatterDate
//
        Keys.status = 2
//
////        sendRequestRequest()
//        sendDeviceStatus()
        startBackgroundTask()
        startBackgroundRequest()
    }
    
    func startBackgroundRequest() {
        let application = UIApplication.shared
        BackgroundTask.run(application: application) { backgroundTask in
            print("end")
            
            let headers = [
                "Cookie":"__cfduid=d9a5e2e9e9dbd3c0ede79fa2179344afe1520780675; ci_session=8808ef4540291fdd60126f42b440e6c1dc5702c7",
                "Content-Type":"application/json; charset=utf-8",
                ]
            let body: [String : Any] = [
                "appcode": Keys.appcode,
                "device_code": Keys.device_code,
                "start_time": Keys.start_time,
                "video_details": Keys.video_details.joined(separator: ","),
                "end_time": Keys.end_time
            ]
            print("Video Records \n")
            print(body)
            Alamofire.request("http://5thfloor.media/app/api.video/n1/Util/video_records", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
                .validate(statusCode: 200..<300)
                .responseJSON { response in
                    if (response.result.error == nil) {
                        print("response success records")
                        Keys.video_details = []
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
                        let formatterDate = dateFormatter.string(from: Date())
                        Keys.start_time = formatterDate
                    }
                    else {
                        print("response failed records")
                    }
                    backgroundTask.end()
            }
        }
    }
    
    func startBackgroundTask() {
        let application = UIApplication.shared
        BackgroundTask.run(application: application) { backgroundTask in
            print("end")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
            let formatterDate = dateFormatter.string(from: Date())
            let headers = [
                "Cookie":"__cfduid=d9a5e2e9e9dbd3c0ede79fa2179344afe1520780675; ci_session=9d8fedc405a3ac6a2b92ec0a199a916c36e91923",
                "Content-Type":"application/json; charset=utf-8",
                ]
            let body: [String : Any] = [
                "status": Keys.status,
                "date_time": formatterDate,
                "appcode": Keys.appcode
            ]
            print("Video Overview \n")
            print(body)
            
            Alamofire.request("http://5thfloor.media/app/api.video/n1/Util/video_overview", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
                .validate(statusCode: 200..<300)
                .responseJSON { response in
                    if (response.result.error == nil) {
                        print("response success overview")
                    }
                    else {
                        print("response failed overview")
                    }
            backgroundTask.end()
            }
            

        }
        print("begin")
    }
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("applicationDidBecomeActive")
        
        Keys.status = 1
        Keys.device_code = UIDevice.current.identifierForVendor!.uuidString
        sendRequestRequest()
        sendDeviceStatus()
        
        if(!Keys.firstTime){
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
            let formatterDate = dateFormatter.string(from: Date())
            Keys.start_time = formatterDate
            
            print("First Time")
            
            Keys.firstTime = true
        }
        
        
        

    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        print("applicationWillTerminate")
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
//        let formatterDate = dateFormatter.string(from: Date())
//        Keys.end_time = formatterDate
//
//        Keys.status = 2
//
//        sendDeviceStatus()
//        sendRequestRequest()
//        backgroundTask.end()

        self.saveContext()
    }


    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "OrangeMediasApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

