//
//  NetworkAdapter.swift
//  VideoApp
//
//  Created by Umer Jabbar on 08/03/2018.
//  Copyright © 2018 ZotionApps. All rights reserved.
//

import Foundation
import Moya

struct NetworkAdapter {
    static let provider = MoyaProvider<MyServerAPI>()
    
    static func request(target: MyServerAPI, success successCallback: @escaping (Response) -> Void, error errorCallback: @escaping (Swift.Error) -> Void, failure failureCallback: @escaping (MoyaError) -> Void) {
        
//        print("rx provider base \(errno)")
//        print("rx provider \(successCallback)")
        
        provider.request(target) { (result) in
            switch result {
            case .success(let response):
                // 1:
                if response.statusCode >= 200 && response.statusCode <= 300 {
                    successCallback(response)
                } else {
                    // 2:
                print(response.statusCode)
                    let error = NSError(domain:"com.eorchids.orangemedias", code:0, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
                    errorCallback(error)
                
                    
                    
                }
            case .failure(let error):
                // 3:
                failureCallback(error)
            }
        }
    }
}

