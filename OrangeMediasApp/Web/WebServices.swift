//
//  WebServices.swift
//  VideoApp
//
//  Created by Umer Jabbar on 08/03/2018.
//  Copyright © 2018 ZotionApps. All rights reserved.
//

import Foundation
import Moya

enum MyServerAPI {
    
    case getVideos
    
}

extension MyServerAPI : TargetType {
    var baseURL: URL {
        return URL(string : "http://5thfloor.media/app/")!
    }
    
    var path: String {
        switch self {
        case .getVideos:
            return "api.video/n1/Video/get_video_category?app_id=om19"

        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getVideos:
            return .get

        }
    }
    
    var sampleData: Foundation.Data {
        return Foundation.Data()
    }
    
    var task: Task {
//        switch self {
//        case .getVideos(let id):
//            return .requestParameters(parameters: ["app_id": id], encoding: JSONEncoding.default)
//        }
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
        
    }
    

    

    
    
    
}

//// MARK: - Helpers
//private extension String {
//    var urlEscaped: String {
//        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
//    }
//    
//    var utf8Encoded: Data {
//        return data(using: .utf8)
//    }
//}


