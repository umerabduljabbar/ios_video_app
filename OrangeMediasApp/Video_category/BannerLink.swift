//
//  BannerLink.swift
//
//  Created by Umer Jabbar on 10/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class BannerLink: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let holdTime = "hold_time"
    static let imgPath = "img_path"
  }

  // MARK: Properties
  public var holdTime: String?
  public var imgPath: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    holdTime <- map[SerializationKeys.holdTime]
    imgPath <- map[SerializationKeys.imgPath]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = holdTime { dictionary[SerializationKeys.holdTime] = value }
    if let value = imgPath { dictionary[SerializationKeys.imgPath] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.holdTime = aDecoder.decodeObject(forKey: SerializationKeys.holdTime) as? String
    self.imgPath = aDecoder.decodeObject(forKey: SerializationKeys.imgPath) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(holdTime, forKey: SerializationKeys.holdTime)
    aCoder.encode(imgPath, forKey: SerializationKeys.imgPath)
  }

}
