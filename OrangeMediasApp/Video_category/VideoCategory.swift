//
//  VideoCategory.swift
//
//  Created by Umer Jabbar on 10/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class VideoCategory: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let data = "data"
    static let breakVideo = "break_video"
    static let bannerLink = "banner_link"
  }

  // MARK: Properties
  public var status: Bool? = false
  public var data: [Data]?
  public var breakVideo: [BreakVideo]?
  public var bannerLink: [BannerLink]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    data <- map[SerializationKeys.data]
    breakVideo <- map[SerializationKeys.breakVideo]
    bannerLink <- map[SerializationKeys.bannerLink]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.status] = status
    if let value = data { dictionary[SerializationKeys.data] = value.map { $0.dictionaryRepresentation() } }
    if let value = breakVideo { dictionary[SerializationKeys.breakVideo] = value.map { $0.dictionaryRepresentation() } }
    if let value = bannerLink { dictionary[SerializationKeys.bannerLink] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeBool(forKey: SerializationKeys.status)
    self.data = aDecoder.decodeObject(forKey: SerializationKeys.data) as? [Data]
    self.breakVideo = aDecoder.decodeObject(forKey: SerializationKeys.breakVideo) as? [BreakVideo]
    self.bannerLink = aDecoder.decodeObject(forKey: SerializationKeys.bannerLink) as? [BannerLink]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(data, forKey: SerializationKeys.data)
    aCoder.encode(breakVideo, forKey: SerializationKeys.breakVideo)
    aCoder.encode(bannerLink, forKey: SerializationKeys.bannerLink)
  }

}
