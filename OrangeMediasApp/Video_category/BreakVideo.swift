//
//  BreakVideo.swift
//
//  Created by Umer Jabbar on 10/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class BreakVideo: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let count = "count"
    static let vUrl = "v_url"
  }

  // MARK: Properties
  public var count: String?
  public var vUrl: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    count <- map[SerializationKeys.count]
    vUrl <- map[SerializationKeys.vUrl]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = count { dictionary[SerializationKeys.count] = value }
    if let value = vUrl { dictionary[SerializationKeys.vUrl] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.count = aDecoder.decodeObject(forKey: SerializationKeys.count) as? String
    self.vUrl = aDecoder.decodeObject(forKey: SerializationKeys.vUrl) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(count, forKey: SerializationKeys.count)
    aCoder.encode(vUrl, forKey: SerializationKeys.vUrl)
  }

}
