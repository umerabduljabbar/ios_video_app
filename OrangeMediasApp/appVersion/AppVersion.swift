//
//  AppVersion.swift
//
//  Created by Umer Jabbar on 11/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class AppVersion: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let getUserAppRelease = "GetUserAppRelease"
  }

  // MARK: Properties
  public var status: Bool? = false
  public var getUserAppRelease: [GetUserAppRelease]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    getUserAppRelease <- map[SerializationKeys.getUserAppRelease]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.status] = status
    if let value = getUserAppRelease { dictionary[SerializationKeys.getUserAppRelease] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeBool(forKey: SerializationKeys.status)
    self.getUserAppRelease = aDecoder.decodeObject(forKey: SerializationKeys.getUserAppRelease) as? [GetUserAppRelease]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(getUserAppRelease, forKey: SerializationKeys.getUserAppRelease)
  }

}
