//
//  GetUserAppRelease.swift
//
//  Created by Umer Jabbar on 11/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class GetUserAppRelease: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let version = "version"
    static let forceupdate = "forceupdate"
    static let id = "id"
    static let releasedate = "releasedate"
  }

  // MARK: Properties
  public var version: String?
  public var forceupdate: String?
  public var id: String?
  public var releasedate: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    version <- map[SerializationKeys.version]
    forceupdate <- map[SerializationKeys.forceupdate]
    id <- map[SerializationKeys.id]
    releasedate <- map[SerializationKeys.releasedate]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = version { dictionary[SerializationKeys.version] = value }
    if let value = forceupdate { dictionary[SerializationKeys.forceupdate] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = releasedate { dictionary[SerializationKeys.releasedate] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.version = aDecoder.decodeObject(forKey: SerializationKeys.version) as? String
    self.forceupdate = aDecoder.decodeObject(forKey: SerializationKeys.forceupdate) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.releasedate = aDecoder.decodeObject(forKey: SerializationKeys.releasedate) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(version, forKey: SerializationKeys.version)
    aCoder.encode(forceupdate, forKey: SerializationKeys.forceupdate)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(releasedate, forKey: SerializationKeys.releasedate)
  }

}
