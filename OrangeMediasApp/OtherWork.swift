//
//  OtherWork.swift
//  OrangeMediasApp
//
//  Created by Umer Jabbar on 11/03/2018.
//  Copyright © 2018 ZotionApps. All rights reserved.
//

import Foundation
//class ViewController: UIViewController {
//    fileprivate var player = Player()
//
//    // MARK: object lifecycle
//    deinit {
//        self.player.willMove(toParentViewController: self)
//        self.player.view.removeFromSuperview()
//        self.player.removeFromParentViewController()
//    }
//
//    // MARK: view lifecycle
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        print("Started")
//
//        videoUrl = URL(fileURLWithPath: downloadVideo(url: videoUrl.absoluteString))
//
//        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//
//
//        self.player.playerDelegate = self
//        self.player.playbackDelegate = self
//        self.player.view.frame = self.view.bounds
//
//
//        self.addChildViewController(self.player)
//        self.view.addSubview(self.player.view)
//        self.player.didMove(toParentViewController: self)
//
////        let resourcePath = Bundle.main.path(forResource: "XHv6SSRG-30344880", ofType:"mp4")
//        self.player.url = NSURL(fileURLWithPath: "/Users/umer/Library/Developer/CoreSimulator/Devices/FFF93CA5-1527-4AD6-9FAC-25CC7728065B/data/Containers/Data/Application/58801EC6-1A73-4C93-ADE5-58B947358EDB/Documents/XHv6SSRG-30344880.mp4") as URL
//
////        self.player.playFromBeginning()
//
////        self.player.playbackLoops = false
//
//        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
//        tapGestureRecognizer.numberOfTapsRequired = 1
//        self.player.view.addGestureRecognizer(tapGestureRecognizer)
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//
//        self.player.playFromBeginning()
//    }
//
//    func getVideos(){
//
////        NetworkAdapter.request(target: .getVideos(id : "om19") , success: { (response) in
////            // parse your data
////            print("success")
////        }, error: { (error) in
////            // show error from server
////            print("error")
////        }, failure: { (error) in
////            // show Moya error
////            print("failure")
////        })
//
//
//
//    }
//
//    func sendRequest2Request() {
//        /**
//         Request (2)
//         get http://5thfloor.media/app/api.video/n1/Video/get_video_category
//         */
//
//        // Add Headers
//        let headers:[String:String] = [:]
//
//        // Add URL parameters
//        let urlParams = [
//            "app_id":"om19",
//            ]
//
//        // Fetch Request
//        Alamofire.request("http://5thfloor.media/app/api.video/n1/Video/get_video_category", method: .get, parameters: urlParams, headers: headers)
//            .validate(statusCode: 200..<300)
//            .responseJSON { response in
//                if (response.result.error == nil) {
//                    debugPrint("HTTP Response Body: \(response.data)")
//
////                    let object = response.result.value as! VideoCategory
////                    object.dictionaryRepresentation()
//
//
//                }
//                else {
//                    debugPrint("HTTP Request failed: \(response.result.error)")
//                }
//        }
//    }
//
//    func downloadVideo(url : String) -> String{
//
//        let videoLink = url
//        guard let videoURL = URL(string: videoLink) else { return ""}
//
//        guard let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return "" }
//
//        // check if the file already exist at the destination folder if you don't want to download it twice
//        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent(videoURL.lastPathComponent).path) {
//
//            // set up your download task
//            URLSession.shared.downloadTask(with: videoURL) { (location, response, error) -> Void in
//
//                // use guard to unwrap your optional url
//                guard let location = location else { return }
//
//                // create a deatination url with the server response suggested file name
//                let destinationURL = documentsDirectoryURL.appendingPathComponent(response?.suggestedFilename ?? videoURL.lastPathComponent)
//
//
//
//                print(destinationURL.absoluteString )
//
//                do {
//
//                    try FileManager.default.moveItem(at: location, to: destinationURL)
//
//                    PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
//
//                        // check if user authorized access photos for your app
//                        if authorizationStatus == .authorized {
//                            PHPhotoLibrary.shared().performChanges({
//                                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: destinationURL)}) { completed, error in
//                                    if completed {
//                                        print("Video asset created")
//
//                                    } else {
//                                        print(error)
//                                    }
//                            }
//                        }
//                    })
//
//                } catch { print(error) }
//
//                }.resume()
//
//        } else {
//            print("File already exists at destination url")
//            return documentsDirectoryURL.appendingPathComponent(videoURL.lastPathComponent).path
//        }
//
//return documentsDirectoryURL.appendingPathComponent(videoURL.lastPathComponent).path
//    }
//
//}
//
//// MARK: - UIGestureRecognizer
//
//extension ViewController {
//
//    @objc func handleTapGestureRecognizer(_ gestureRecognizer: UITapGestureRecognizer) {
//        switch (self.player.playbackState.rawValue) {
//        case PlaybackState.stopped.rawValue:
//            self.player.playFromBeginning()
//            break
//        case PlaybackState.paused.rawValue:
//            self.player.playFromCurrentTime()
//            break
//        case PlaybackState.playing.rawValue:
//            self.player.pause()
//            break
//        case PlaybackState.failed.rawValue:
//            self.player.pause()
//            break
//        default:
//            self.player.pause()
//            break
//        }
//    }
//
//}
//
//// MARK: - PlayerDelegate
//
//extension ViewController:PlayerDelegate {
//
//    func playerReady(_ player: Player) {
//    }
//
//    func playerPlaybackStateDidChange(_ player: Player) {
//    }
//
//    func playerBufferingStateDidChange(_ player: Player) {
//    }
//    func playerBufferTimeDidChange(_ bufferTime: Double) {
//
//    }
//
//}
//
//// MARK: - PlayerPlaybackDelegate
//
//extension ViewController:PlayerPlaybackDelegate {
//
//    func playerCurrentTimeDidChange(_ player: Player) {
//    }
//
//    func playerPlaybackWillStartFromBeginning(_ player: Player) {
//    }
//
//    func playerPlaybackDidEnd(_ player: Player) {
//    }
//
//    func playerPlaybackWillLoop(_ player: Player) {
//    }
//
//}
//
