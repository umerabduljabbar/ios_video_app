//
//  PlayerViewController.swift
//  OrangeMediasApp
//
//  Created by Umer Jabbar on 11/03/2018.
//  Copyright © 2018 ZotionApps. All rights reserved.
//

import UIKit
import AVKit
import Player
import Kingfisher
import Alamofire


class PlayerViewController: UIViewController {
    
    var pathArray = [String]()
    var count = 0
    var image:String?
    var player = Player()
    var time = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPlayer()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    
    func sendDeviceStatus() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
        let formatterDate = dateFormatter.string(from: Date())
        let headers = [
            "Cookie":"__cfduid=d9a5e2e9e9dbd3c0ede79fa2179344afe1520780675; ci_session=9d8fedc405a3ac6a2b92ec0a199a916c36e91923",
            "Content-Type":"application/json; charset=utf-8",
            ]
        let body: [String : Any] = [
            "status": Keys.status,
            "date_time": formatterDate,
            "appcode": Keys.appcode
        ]
        print("Video Overview \n")
        print(body)
        
        Alamofire.request("http://5thfloor.media/app/api.video/n1/Util/video_overview", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    print("response success overview")
                }
                else {
                    print("response failed overview")
                }
        }
    }
    func sendRequestRequest() {
        let headers = [
            "Cookie":"__cfduid=d9a5e2e9e9dbd3c0ede79fa2179344afe1520780675; ci_session=54a88976bbda2278c735d3643ae4a22dc03b81a2",
            "Content-Type":"application/json; charset=utf-8",
            ]
        let body: [String : Any] = [
            "appcode": Keys.appcode,
            "device_code": Keys.device_code,
            "start_time": Keys.start_time,
            "video_details": Keys.video_details.joined(separator: ","),
            "end_time": Keys.end_time
        ]
        print("Video Records \n")
        print(body)
        Alamofire.request("http://5thfloor.media/app/api.video/n1/Util/video_records", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    print("response success records")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
                    let formatterDate = dateFormatter.string(from: Date())
                    Keys.start_time = formatterDate
                }
                else {
                    print("response failed records")
                }
        }
        Keys.video_details = []
    }
    
    @objc func appMovedToBackground() {
        print("App moved to background!")
//        self.player.stop()
//        Keys.status = 2
//        sendDeviceStatus()
    }
    
    @objc func appMovedToForeground() {
//        self.player.playFromCurrentTime()
        print("App moved to Foreground!")
    }
    
    func setupPlayer(){
//        player = Player()
        self.player.playerDelegate = self
        self.player.playbackDelegate = self
        self.player.view.frame = self.view.bounds
        
        
        self.addChildViewController(self.player)
        self.view.addSubview(self.player.view)
        self.player.didMove(toParentViewController: self)
        
//        self.count = Keys.count
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
        let formatterDate = dateFormatter.string(from: Date())
        Keys.start_time = formatterDate
        print(formatterDate)
        
        if(!self.pathArray.isEmpty){
            let videoUrl: URL = NSURL(fileURLWithPath: self.pathArray[count]) as URL
            self.player.url = videoUrl
            self.player.playFromBeginning()
            
        }
        
        
        self.player.fillMode = PlayerFillMode.resizeAspectFit.avFoundationType
        
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: self.view.frame.height/2-20, width: self.view.frame.width, height: self.view.frame.height))
        //        imageView.center = CGPoint(x: 0, y: self.view.frame.width)
        imageView.contentMode = .scaleAspectFit
        player.view.addSubview(imageView)
        let url = URL(string: self.image ?? "")
        imageView.kf.setImage(with: url)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presentingViewController?.dismiss(animated: false)
        print("viewDidAppear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension PlayerViewController : PlayerDelegate{
    
    func playerReady(_ player: Player) {
        print("Player Ready")

    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferTimeDidChange(_ bufferTime: Double) {
        
    }
    
    
}

extension PlayerViewController : PlayerPlaybackDelegate{
    
    func playerCurrentTimeDidChange(_ player: Player) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
        let formatterDate = dateFormatter.string(from: Date())
        Keys.end_time = formatterDate
        Keys.seekTime = Double(player.currentTime)
        time += 1
        
//        print(formatterDate)
        
        if(time%30000 == 0){
            Keys.status = 1
            sendDeviceStatus()
            time = 0
        }
    }
    
    func playerPlaybackWillStartFromBeginning(_ player: Player) {
        
    }
    
    func playerPlaybackDidEnd(_ player: Player) {
        print("Player playback Did End")

        Keys.video_details.append(NSURL(fileURLWithPath: self.pathArray[count]).lastPathComponent!)
        print("video Added \(Keys.video_details.last ?? "nothing")")
        
        count += 1
        
        if(!self.pathArray.isEmpty){
            if(self.pathArray.count > count){
                self.player.url = NSURL(fileURLWithPath: self.pathArray[count]) as URL
                self.player.playFromBeginning()
            }else{
                count = 0
                self.player.url = NSURL(fileURLWithPath: self.pathArray[count]) as URL
                self.player.playFromBeginning()
            }
        }
    }
    
    func playerPlaybackWillLoop(_ player: Player) {
        
    }
    
    
}
