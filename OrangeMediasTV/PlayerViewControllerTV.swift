//
//  PlayerViewControllerTV.swift
//  OrangeMediasTV
//
//  Created by Umer Jabbar on 11/03/2018.
//  Copyright © 2018 ZotionApps. All rights reserved.
//

import UIKit
import AVKit
import Player
import Kingfisher
import Alamofire

class PlayerViewControllerTV: UIViewController {
    
    var pathArray = [String]()
    var count = 0
    var image:String?
    var player = Player()
    var time = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPlayer()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    
    func sendDeviceStatus() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
        let formatterDate = dateFormatter.string(from: Date())
        let headers = [
            "Cookie":"__cfduid=d9a5e2e9e9dbd3c0ede79fa2179344afe1520780675; ci_session=9d8fedc405a3ac6a2b92ec0a199a916c36e91923",
            "Content-Type":"application/json; charset=utf-8",
            ]
        let body: [String : Any] = [
            "status": Keys.status,
            "date_time": formatterDate,
            "appcode": Keys.appcode
        ]
        print("Video Overview \n")
        print(body)
        
        Alamofire.request("http://5thfloor.media/app/api.video/n1/Util/video_overview", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    print("response success overview")
                }
                else {
                    print("response failed overview")
                }
        }
    }
    
    @objc func appMovedToBackground() {
        print("App moved to background!")
        //        self.player.stop()
//        Keys.status = 2
//        sendDeviceStatus()
    }
    
    @objc func appMovedToForeground() {
        //        self.player.playFromCurrentTime()
        print("App moved to Foreground!")
    }
    
    func setupPlayer(){
        //        player = Player()
        self.player.playerDelegate = self
        self.player.playbackDelegate = self
        self.player.view.frame = self.view.bounds
        
        
        self.addChildViewController(self.player)
        self.view.addSubview(self.player.view)
        self.player.didMove(toParentViewController: self)
        
//        self.count = Keys.count
        
        if(!self.pathArray.isEmpty){
            let videoUrl: URL = NSURL(fileURLWithPath: self.pathArray[count]) as URL
            self.player.url = videoUrl
//            self.count += 1
            self.player.playFromBeginning()
        }
        
        
        self.player.fillMode = PlayerFillMode.resizeAspectFit.avFoundationType
        
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: self.view.frame.height/2-55, width: self.view.frame.width, height: self.view.frame.height))
        
//        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        //
        let url = URL(string: self.image ?? "")
        imageView.kf.setImage(with: url)
        player.view.addSubview(imageView)
        
//        let centerXConst = NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: player.view, attribute: .centerX, multiplier: 1.0, constant: 0.0)
//        let centerYConst = NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: player.view, attribute: .centerY, multiplier: 1.0, constant: 0.0)
//
//        let heightConstraint = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0.0)
//        let widthConstraint = NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0.0)
//        imageView.addConstraints([heightConstraint, widthConstraint])
//
//        NSLayoutConstraint.activate([centerXConst, centerYConst])
        
        //        imageView.center = CGPoint(x: 0, y: self.view.frame.width)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presentingViewController?.dismiss(animated: false)
        print("viewDidAppear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension PlayerViewControllerTV : PlayerDelegate{
    
    func playerReady(_ player: Player) {
        print("Player Ready")
    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferTimeDidChange(_ bufferTime: Double) {
        
    }
    
    
}

extension PlayerViewControllerTV : PlayerPlaybackDelegate{
    
    func playerCurrentTimeDidChange(_ player: Player) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
        let formatterDate = dateFormatter.string(from: Date())
        Keys.end_time = formatterDate
        time += 1
        
        if(time%30000 == 0){
            Keys.status = 1
            sendDeviceStatus()
            time = 0
        }
    }
    
    func playerPlaybackWillStartFromBeginning(_ player: Player) {
        
    }
    
    func playerPlaybackDidEnd(_ player: Player) {
        print("Player playback Did End")
        
        Keys.video_details.append(NSURL(fileURLWithPath: self.pathArray[count]).lastPathComponent!)
        
        count += 1
        
        if(!self.pathArray.isEmpty){
//            Keys.count = count
            if(self.pathArray.count > count){
                self.player.url = NSURL(fileURLWithPath: self.pathArray[count]) as URL
                self.player.playFromBeginning()
            }else{
                count = 0
                self.player.url = NSURL(fileURLWithPath: self.pathArray[count]) as URL
                self.player.playFromBeginning()
            }
        }
        
    }
    
    func playerPlaybackWillLoop(_ player: Player) {
        
    }
    
    
}
