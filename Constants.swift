//
//  Constants.swift
//  OrangeMediasTV
//
//  Created by Umer Jabbar on 11/03/2018.
//  Copyright © 2018 ZotionApps. All rights reserved.
//

import Foundation


class Keys{
    
    static var pathLink : [String] {
        get{
            return getStringArray(name: .pathLink)
        }
        set{
            save(name: .pathLink, value: newValue)
        }
    }
    
    static var video_details : [String] {
        get{
            return getStringArray(name: .video_details)
        }
        set{
            save(name: .video_details, value: newValue)
        }
    }
    
    static var imageLink : String {
        get{
            return getString(name: .imageLink)
        }
        set{
            save(name: .imageLink, value: newValue)
        }
    }
    
    static var appcode : String {
        get{
            return getString(name: .appcode)
        }
        set{
            save(name: .appcode, value: newValue)
        }
    }
    
    static var start_time : String {
        get{
            return getString(name: .start_time)
        }
        set{
            save(name: .start_time, value: newValue)
        }
    }
    
    static var end_time : String {
        get{
            return getString(name: .end_time)
        }
        set{
            save(name: .end_time, value: newValue)
        }
    }
    
    static var device_code : String {
        get{
            return getString(name: .device_code)
        }
        set{
            save(name: .device_code, value: newValue)
        }
    }
    
    static var status : Int {
        get{
            return getInt(name: .status)
        }
        set{
            save(name: .status, value: newValue)
        }
    }
    
    static var installed : Bool {
        get{
            return getBool(name: .installed)
        }
        set{
            save(name: .installed, value: newValue)
        }
    }
    
    static var count : Int {
        get{
            return getInt(name: .count)
        }
        set{
            save(name: .count, value: newValue)
        }
    }
    
    static var totalVideoCount : Int {
        get{
            return getInt(name: .totalVideoCount)
        }
        set{
            save(name: .totalVideoCount, value: newValue)
        }
    }
    
    static var downloadedVideos : Int {
        get{
            return getInt(name: .downloadedVideos)
        }
        set{
            save(name: .downloadedVideos, value: newValue)
        }
    }
    
    static var firstTime : Bool {
        get{
            return getBool(name: .firstTime)
        }
        set{
            save(name: .firstTime, value: newValue)
        }
    }
    
    static var continueDownload : Bool {
        get{
            return getBool(name: .continueDownload)
        }
        set{
            save(name: .continueDownload, value: newValue)
        }
    }
    
    static var seekTime : Double {
        get{
            return getDouble(name: .seekTime)
        }
        set{
            save(name: .seekTime, value: newValue)
        }
    }
    
    class func save(name : Name, value : Any)  {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: name.rawValue)
    }
    class func getInt(name : Name) -> Int{
        let defaults = UserDefaults.standard
        return defaults.integer(forKey: name.rawValue)
    }
    class func getDouble(name : Name) -> Double{
        let defaults = UserDefaults.standard
        return defaults.double(forKey: name.rawValue)
    }
    class func getString(name : Name) -> String{
        let defaults = UserDefaults.standard
        return defaults.string(forKey: name.rawValue) ?? ""
    }
    class func getBool(name : Name) -> Bool{
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: name.rawValue)
    }
    class func getStringArray(name : Name) -> [String]{
        let defaults = UserDefaults.standard
        return defaults.stringArray(forKey: name.rawValue) ?? []
    }
    
}

enum Name : String  {
    case totalVideoCount = "totalVideoCount"
    case seekTime = "seekTime"
    case downloadedVideos = "downloadedVideos"
    case count = "count"
    case video_details = "video_details"
    case start_time = "start_time"
    case end_time = "end_time"
    case appcode = "appcode"
    case device_code = "device_code"
    case status = "status"
    case pathLink = "pathLink"
    case installed = "installed"
    case imageLink = "imageLink"
    case firstTime = "firstTime"
    case continueDownload = "continueDownload"
    
}

